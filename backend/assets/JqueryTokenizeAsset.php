<?php

namespace backend\assets;
 
use yii\web\AssetBundle;
class JqueryTokenizeAsset extends AssetBundle
{
    // The files are not web directory accessible, therefore we need 
    // to specify the sourcePath property. Notice the @vendor alias used.
    public $sourcePath = '@bower/jquery-tokenize';
    public $css = [
        'jquery.tokenize.css',
    ];
    public $js = [
        'jquery.tokenize.js'
    ];
}
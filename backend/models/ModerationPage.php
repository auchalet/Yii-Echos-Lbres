<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "moderation_page".
 *
 * @property integer $id
 * @property string $message
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $page_id
 * @property integer $user_id
 *
 * @property Page $page
 * @property User $user
 */
class ModerationPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'moderation_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'page_id', 'user_id'], 'required'],
            [['status', 'page_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'page_id' => 'Page ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

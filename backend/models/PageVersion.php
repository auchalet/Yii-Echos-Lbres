<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "page_version".
 *
 * @property integer $id
 * @property string $body
 * @property string $titre_principal
 * @property string $sous_titre
 * @property integer $status
 * @property string $created_at
 * @property integer $page_id
 * @property string $message
 * @property integer $user_id
 *
 * @property Page $page
 * @property User $user
 */
class PageVersion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body', 'titre_principal', 'sous_titre', 'status', 'page_id', 'message', 'user_id'], 'required'],
            [['body'], 'string'],
            [['status', 'page_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['titre_principal', 'sous_titre'], 'string', 'max' => 200],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Body',
            'titre_principal' => 'Titre Principal',
            'sous_titre' => 'Sous Titre',
            'status' => 'Status',
            'created_at' => 'Created At',
            'page_id' => 'Page ID',
            'message' => 'Message',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

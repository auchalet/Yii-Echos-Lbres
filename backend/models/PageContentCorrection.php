<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "page_content_correction".
 *
 * @property integer $id
 * @property string $titre_principal
 * @property string $sous_titre
 * @property string $body
 * @property string $created_at
 * @property integer $page_id
 * @property string $message
 * @property integer $user_id
 *
 * @property Page $page
 * @property User $user
 */
class PageContentCorrection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_content_correction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titre_principal', 'sous_titre', 'body', 'page_id', 'message', 'user_id'], 'required'],
            [['body'], 'string'],
            [['created_at'], 'safe'],
            [['page_id', 'user_id'], 'integer'],
            [['titre_principal', 'sous_titre'], 'string', 'max' => 200],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titre_principal' => 'Titre Principal',
            'sous_titre' => 'Sous Titre',
            'body' => 'Body',
            'created_at' => 'Created At',
            'page_id' => 'Page ID',
            'message' => 'Message',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

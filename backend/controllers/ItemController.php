<?php

namespace backend\controllers;

use Yii;
use common\models\ItemType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use common\models\Item;

/**
 * ItemController implements the CRUD actions for ItemType model.
 */
class ItemController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ItemType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ItemType::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ItemType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $item_type = $this->findModel($id);
                       
        return $this->render('@frontend/views/'.$item_type->view, ['item_type' => $item_type]);
       
    }

    /**
     * Creates a new ItemType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ItemType();
        
        /**
         * TODO : Récupérer les vues d'items / Exclure index, update, create et _form
         */
        
        

        $viewFiles=FileHelper::findFiles('../../frontend/views/item/', [
            'except' => ['create']
        ]);

        
//        var_dump($viewFiles);die;
        $views = array();
        
        if (isset($viewFiles[0])) {
            foreach ($viewFiles as $k => $v) {
                $nameExt = substr($v, strrpos($v, '/') + 1);
                $name = substr($nameExt, 0, strlen($nameExt) - 4);
                                
                $action = 'item/'.$name;
                $path = realpath($v);
                
                $views[$k]['name'] = $name;
                $views[$k]['action'] = $action;
                $views[$k]['path'] = $path;
                
            }
        }       
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $dataProvider = new ActiveDataProvider([
                'query' => ItemType::find(),
            ]);

            return $this->redirect('index', [
                'dataProvider' => $dataProvider,
            ]);
                
        }
        else {
            return $this->render('create', [
                'model' => $model,
                'views' => $views
            ]);
        }
    }

    /**
     * Updates an existing ItemType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $viewFiles=FileHelper::findFiles('../../frontend/views/item/', [
            'except' => ['create']
        ]);

        
//        var_dump($viewFiles);die;
        $views = array();
        
        if (isset($viewFiles[0])) {
            foreach ($viewFiles as $k => $v) {
                $nameExt = substr($v, strrpos($v, '/') + 1);
                $name = substr($nameExt, 0, strlen($nameExt) - 4);
                                
                $action = 'item/'.$name;
                $path = realpath($v);
                
                $views[$k]['name'] = $name;
                $views[$k]['action'] = $action;
                $views[$k]['path'] = $path;
                
            }
        }         
        
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $dataProvider = new ActiveDataProvider([
                'query' => ItemType::find(),
            ]);

            return $this->redirect('index', [
                'dataProvider' => $dataProvider,
            ]);            
            
        } else {
            return $this->render('update', [
                'model' => $model,
                'views' => $views                
            ]);
        }
    }

    /**
     * Deletes an existing ItemType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /**
     * Prévisualise un item_type créé
     * TODO : Récupérer le formulaire / Construire l'objet Item et rendre la vue correspondante
     */
    public function actionPreview()
    {
        
        $item_type = new ItemType;
        
        $array = Yii::$app->request->post('item');
        array_shift($array);
        foreach($array as $v) {
//            var_dump($v);
            preg_match('/\[\w*]/', $v['name'], $matches);
            
            $name = str_replace('[', '', $matches);
            $name = str_replace(']', '', $name);
            
//            var_dump($name);
            $item_type->{$name[0]} = $v['value'];
            
            
        }
        
        
//        var_dump($item_type->view);die;
        
        return $this->renderPartial('@frontend/views/'.$item_type->view, ['item_type' => $item_type]);
        
        
    }
    

    /**
     * Finds the ItemType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ItemType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ItemType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

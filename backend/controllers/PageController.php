<?php

namespace backend\controllers;

use Yii;
use common\models\Page;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\Tag;
use common\models\PageCategory;
use common\models\PageTag;
use common\models\PageContent;
use common\models\PageFile;
use common\models\Source;
use common\models\UploadFile;
use common\models\UploadForm;


/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        //TODO : lister les actions possibles sur les pages (édition, commentaires, modération, liste etc.)
        //Essayer de gérer blocs avec Bootstrap

        return $this->render('index');
    }

    /**
     * Liste les catégories de pages
     * @return mixed
     */
    public function actionListPages($id_category) {
        $category = PageCategory::getById($id_category);
        $pages = $category->getPages()->all();

        $users = array();
        $tags = array();
        if ($pages !== null) {
            foreach ($pages as $v) {
                $users[] = User::findId($v->user_id);
                $tags[] = $v->getTags()->all();
                //Récupération binaire des statuts 1 = OK ; 0 = !OK // SUPPRIME : on reste sur 1-2-3-4-5
//                $status_bin = str_split((string)$v->status);
//                $status[] = $status_bin;
            }
        }


        return $this->render('list-pages', [
                    'category' => $category,
                    'pages' => $pages,
                    'users' => $users,
                    'tags' => $tags
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewPage($id) {
        return $this->render('view-page', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatePage() {
        
        $category = new PageCategory();
        
        
        $model = new Page();
        
        $content = new PageContent();
        $upload = new UploadForm;

        $user = Yii::$app->user;


        $categories = PageCategory::find()->all();
        $array_categories = array();
        $array_categories = ArrayHelper::map($categories, 'id', 'title');
        $array_categories[0] = 'Nouvelle catégorie...';


        $tags = Tag::find()->all();
        $sources = Source::find()->all();

        //Soumission du formulaire : Si nouvelle catégorie, la créer puis sauver la page et son contenu (attention à l'ordre)
        //Finir par les sources et les tags et les images            
        if ($category->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $content->load(Yii::$app->request->post()) && $upload->load(Yii::$app->request->post())) {
            
            //Path pour upload 
            Yii::$app->params['uploadPath'] = Yii::getAlias('@frontend').'/web/uploads/';        
            
            $path = Yii::getAlias('@frontend').'/web/uploads/library/images';     
  
            $file = UploadedFile::getInstance($upload, 'imageFile');
            
            $upload->imageFile = $file;
            

            
            if($model->category_id === '0') {
                
                if($category->getByTitle($category->title) == true) {
//                    var_dump($category);die;
                    $category = PageCategory::getByTitle($category->title);
                    $model->category_id = $category->id;
                } else {
                    $category->status = 1;

                    try {
                        $category->save();
                        $model->category_id = $category->id;

                    } catch (Exception $ex) {
                        throw new \yii\web\HttpException(405, 'Error saving model PageCategory');                     
                    }
                }

            }
//            echo 'Catégorie<br>';
//            var_dump($category);
            
            if($model->meta_title == null) {
                $model->meta_title = $model->name;
            }
            
            if($model->slug == null) {
                $slugLower = strtolower($model->name);
                $slugDefault = str_replace(' ', '-', $slugLower);
                $model->slug = $slugDefault;
            }
            
            
            try {
//                echo '<br>Page<br>';
//                var_dump($model);die;
                $model->save();
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(405, 'Error saving model Page');                     
            }         
            
            
            $content->page_id = $model->id;
            
            try {
                $content->save();
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(405, 'Error saving model PageContent');                     
            }      
            
            //Gestion des tags
            $tagsPage = array();

            if(Yii::$app->request->post('tagsPage')) {
                $tagsPage = Yii::$app->request->post('tagsPage');
            }
            
            foreach($tagsPage as $k => $v) {
                $pageTag = new PageTag();

                if((is_int($v) && Tag::getById($v) === false) || (is_string($v) && Tag::getByTitle($v) === false)) {
                    try {
                        
                        $tag = new Tag();
                        $tag->title = $v;
                        $tag->save();

                        $pageTag->tag_id = $tag->id;
                        
                    } catch (Exception $ex) {
                        throw new \yii\web\HttpException(405, 'Error saving model Tag');                     
                    }
                    
                } else {
                    $pageTag->tag_id = $v;
                }
                
                $pageTag->page_id = $model->id;

                try {
                    $pageTag->save();

                   
                    
                } catch (Exception $ex) {
                    throw new \yii\web\HttpException(405, 'Error saving model PageTag');                     
                }                
                
                
            }
            
     
            
            try {
                $id_file = $upload->upload($path);
                
                $pageFile = new PageFile();
                $pageFile->page_id = $model->id;
                $pageFile->file_id = $id_file;
                
                $pageFile->save();
                
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(405, 'Error saving file');                     
            }
        
//            var_dump($model->category_id);die;
            
            Yii::$app->getSession()->setFlash(
                'success','Nouvelle page créée'
            );  
            return $this->redirect(['list-pages', 'id_category' => $model->category_id]);

        } else {
            return $this->render('create-page', [
                'model' => $model,
                'user' => $user,
                'tags' => $tags,
                'category' => $category,
                'categories' => $array_categories,
                'content' => $content,
                'sources' => $sources,
                'upload' => $upload
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $typeItemPage = array();


        $tags_page = new PageTag;
//        $tags_page = $model->getTags();

        $items_type = ItemType::find()->all();
        $items_page = PageItem::getItemsParPage($id);

        foreach ($items_page as $k) {
            $typeItemPage[] = $k->getItemType();
        }


        $tags = Tag::getAllTitle();

        $tags_title = array();
        foreach ($tags as $v) {
            array_push($tags_title, $v['title']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        } else {


            return $this->render('update', [
                        'model' => $model,
                        'tags' => $tags_title,
                        'items_type' => $items_type,
                        'tags_page' => $tags_page,
                        'items_page' => $items_page,
                        'type_items_page' => $typeItemPage
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Crée une Site_Category : nouvelle rubrique de pages du site
     * Si Succès, redirige vers la création d'une page
     * @return mixed
     */
    public function actionCreateCategory() {
        $model = new PageCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create-page', 'id_category' => $model->id]);
        } else {
            return $this->render('create-category', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Ajoute un nouvel page_item vide
     */
    public function actionNewItemPage() {

        if (Yii::$app->request->isPost) {

            $form_post = Yii::$app->request->post();
            $form_post['page_id'] = (int) $form_post['page_id'];
            $form_post['item_id'] = (int) $form_post['item_id'];


            //Instantiation du nouvel Item Page
            $pageItem = new PageItem;
            $pageItem->position = $pageItem->getLastPosition($form_post['page_id']) + 1;
            $pageItem->content = "";
            $pageItem->page_id = $form_post['page_id'];
            $pageItem->item_type_id = $form_post['item_id'];

            if ($pageItem->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new \yii\web\BadRequestHttpException;
        }
    }

}

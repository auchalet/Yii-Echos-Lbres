<?php

namespace backend\controllers;

use Yii;
use common\models\Page;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use common\models\UploadForm;
use common\models\UploadFile;

class FileController extends \yii\web\Controller
{
    
    /**
     * Affiche une gallerie d'images
     * @return type
     * @throws \yii\web\HttpException
     */
    public function actionImageGallery()
    {
        //Formulaires
        $upload = new UploadForm;
        $image = new UploadFile;
        
        
        Yii::$app->params['uploadPath'] = Yii::getAlias('@frontend').'/web/uploads/';        
        
        $images = UploadFile::findImages();
//        var_dump($images);die;
        $items = array();
       
        foreach($images as $k => $v) {
            $items[$k]['url'] = Yii::$app->urlManagerFrontEnd->createUrl('/uploads/library/images/'.$v->filename);
            $items[$k]['src'] = Yii::$app->urlManagerFrontEnd->createUrl('/uploads/library/images/'.$v->filename);
            $items[$k]['options']['title'] = $v->name;

            
        }
        
        
        if($upload->load(Yii::$app->request->post()) && $image->load(Yii::$app->request->post())) {
            
            //Path pour upload 
            Yii::$app->params['uploadPath'] = Yii::getAlias('@frontend').'/web/uploads/';        
            
            $path = Yii::getAlias('@frontend').'/web/uploads/library/images';     
  
            $file = UploadedFile::getInstance($upload, 'imageFile');
            
            $upload->imageFile = $file;            
            
            try {
                $upload->upload($path, $image->name, $image->alt);
                
                return $this->redirect(['image-gallery', [
                        'images' => $images,
                        'items' => $items,
                        'upload' => $upload,
                        'image' => $image
                ]]);                
//                $image->addFile($file);
                
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(405, 'Error saving file');                     
            }
           
        }
        
        //Faire une vue pour le formulaire File + Form
        //Changer l'attribut "Tag" => page_file parce qu'une image peut avoir plusieurs alt
        
        return $this->render('image-gallery', [
            'images' => $images,
            'items' => $items,
            'upload' => $upload,
            'image' => $image
        ]);
    }

    /**
     * Rend la vue permettant de choisir une image
     * Formualaire d'ajout d'image validé en AJAX
     */
    public function actionImagePicker()
    {
        //Formulaires
        $upload = new UploadForm;
        $image = new UploadFile;
        
        
        Yii::$app->params['uploadPath'] = Yii::getAlias('@frontend').'/web/uploads/';        
        
        $images = UploadFile::findImages();
//        var_dump($images);die;
        $items = array();
       
        foreach($images as $k => $v) {
            $items[$k]['url'] = Yii::$app->urlManagerFrontEnd->createUrl('/uploads/library/images/'.$v->filename);
           
        }
        
        
        //Validation du formulaire d'ajout à la page / article
        if($upload->load(Yii::$app->request->post()) && $image->load(Yii::$app->request->post())) {
            
        }
        
        return $this->renderAjax('image-picker', [
            'images' => $images,
            'items' => $items,
            'upload' => $upload,
            'image' => $image
        ]);        
        
        
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }

}

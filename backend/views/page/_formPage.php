<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\JqueryTokenizeAsset;
use yii\jui\AutoComplete;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use common\models\ItemType;
use common\widgets\Editor;


/* @var $this yii\web\View */
/* @var $model common\models\SitePage */
/* @var $form yii\widgets\ActiveForm */
//var_dump($categories);die;
?>
<pre>Faire des blocs qu'on peut cacher en JS // En bas, bloc SEO affiché pour le membre SEO (metas/slug)</pre>
<div class="site-page-form">

    <?php $form = ActiveForm::begin(['id' => 'page-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>   

    <div id="bloc-title" class='bloc-cms'>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nom de la page') ?>
    </div>
    
    
    <div id="bloc-category" class='bloc-cms'>    
        <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => '--- Choisir une catégorie ---', 'id' => 'page-category']) ?>
        
        <div class="form-group form-category" style="display:none;">
            <?= $form->field($category, 'title', ['enableClientValidation' => false])->textInput(['maxlength' => true])->label('Nom de la catégorie') ?>
            <?= $form->field($category, 'description', ['enableClientValidation' => false])->textArea(['maxlength' => true])->label('Courte description') ?>
            
        </div>
    </div>    



    <div id="bloc-content" class='bloc-cms'>
        <i class="fa fa-arrow-up"></i>
        <p>Contenu</p>
            <div class="form-module">

        <?= $form->field($content, 'titre_principal')->textInput(['maxlength' => true])->label('Titre principal') ?>

        <?= $form->field($content, 'sous_titre')->textArea(['maxlength' => true])->label('Sous-titre / Introduction') ?>

        <?=
        $form->field($content, 'body')->widget(Editor::className(), [
            'options' => ['rows' => 6],
            'language' => 'fr_FR',

        ])
        ?>
            </div>
    </div>


    <?php if (Yii::$app->user->can('seo')): ?>
        <div id="bloc-seo" class='bloc-cms'>
            <i class="fa fa-arrow-down"></i>
            <p>Options SEO</p>
            <div class="form-module" style="display:none;">


                <?= $form->field($model, 'meta_title', ['enableClientValidation' => false])->textInput(['maxlength' => true])->label('Titre de la page') ?>
                <?= $form->field($model, 'slug', ['enableClientValidation' => false])->textInput(['maxlength' => true])->label('URL') ?>
                <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>
            </div> 
        </div>
    <?php endif; ?>


    <!-- Champs à masquer --> 
    <?php if (!isset($model->id)): ?>
        <?= $form->field($model, 'status')->hiddenInput(['value' => 1])->label(false); ?>    
    <?php else: ?>
        <?= $form->field($model, 'status')->hiddenInput(['value' => $model->status])->label(false); ?>        
    <?php endif; ?>



    <?= $form->field($model, 'user_id')->hiddenInput(['value' => $user->id])->label(false) ?>


    <div id="bloc-tags" class="bloc-cms">
        <i class="fa fa-arrow-down"></i>
        <p>Tags</p>       
        <div class="form-module" style="display:none;">
            <select id="tokenize" multiple="multiple" class="tokenize-sample" name="tagsPage[]">
                <?php foreach($tags as $key => $val): ?>
                <option value="<?= $val['id'] ?>"><?= $val['title'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div id="bloc-img" class="bloc-cms">
        <i class="fa fa-arrow-down"></i>
        <p>Image principale</p>               
        <div class="form-module">        

            <?= $form->field($upload, 'imageFile')->fileInput()->label('Ajouter une image') ?>
            <br>
            <p>ou parcourez la galerie</p>
            <button id="open-gallery" class="btn btn-action" value="<?= Url::to(['file/image-picker']) ?>">Ouvrir la galerie</button>
        </div>
    </div> 


    <div id="bloc-options" class="bloc-cms">
        <i class="fa fa-arrow-down"></i>
        <p>Options</p>
        <div class="form-module" style="display:none;">
            <?= $form->field($model, 'allow_comment')->checkbox() ?>
            
            <label>Ajouter des sources</label>
        </div>


    </div> 



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>


<!-- Popup en JS pour afficher la gallerie -->
<?php

    Modal::begin([
        'id' => 'modal-image-gallery',
        'size' => Modal::SIZE_LARGE

    ]);

    echo '<div id="gallery-popup" class="container-fluid"></div>';

    Modal::end();

?>   



<?php
//Bug JS sur Drag and Drop

$this->registerJs(
        "$(document).ready(function(e){e.preventDefault;
            $('.fa').click(function(){
                if($(this).hasClass('fa-arrow-down')){
                    $(this).nextAll('.form-module').css('display','block');
                    $(this).switchClass('fa-arrow-down', 'fa-arrow-up', 'slow');
                } else if($(this).hasClass('fa-arrow-up')) {
                    $(this).nextAll('.form-module').css('display','none');
                    $(this).switchClass('fa-arrow-up', 'fa-arrow-down', 'slow');
                }       
            });
            
            $('#page-category').change(function(e){
                e.preventDefault();
                console.log($(this).val());
                if($(this).val() === '0') {
                    $('.form-category').show();
                } else if($('.form-category').css('display') == 'block') {
                    $('.form-category').hide();
                }
            });
            
            $('#tokenize').tokenize();
        });
        
"
);


?>
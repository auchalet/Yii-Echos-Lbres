<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h1>Gestion des utilisateurs</h1>

<ul class="nav nav-tabs nav-justified">
    <li><a href="<?= Url::to(['user/list']) ?>">Liste des membres</a></li>
</ul>

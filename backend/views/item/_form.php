<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ItemType */
/* @var $form yii\widgets\ActiveForm */
$viewsName=ArrayHelper::map($views, 'action', 'action');
?>

<div class="item-type-form">

    <?php $form = ActiveForm::begin(['id' => 'item-form']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'view')->dropDownList($viewsName) ?>

    <?= $form->field($model, 'input')->textInput(['maxlength' => true]) ?>    
    
    <?= $form->field($model, 'attribute_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'attribute_class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'options')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::Button('Prévisualiser', ['class' => 'btn btn-primary preview-button', 'name' => 'preview', 'value' => Url::to(['item/preview', 'item'=>'eho'])]) ?>                
    </div>

    

    <?php ActiveForm::end(); ?>

</div>


<?php

    Modal::begin([
        'id' => 'modal-preview',

    ]);

    echo '<div id="preview-popup"></div>';

    Modal::end();

?>   

<?php

//Prévisualisation
$this->registerJs('', static::POS_READY);

?>
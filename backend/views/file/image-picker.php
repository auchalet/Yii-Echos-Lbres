<?php

use yii\helpers\Html;
use yii\web\UrlManager;
?>

<div class="container-fluid">
    
<?= $this->render('_formFile', [
    'upload' => $upload,
    'file' => $image
])
?>
    
    <hr>  <span style="text-align: center;">ou</span><hr>

    <h2>Sélectionnez un fichier</h2>
    
    <div class="row">   
    <?php foreach($items as $k => $v): ?>
        
            <div class="col-md-2 gallery-item-pick">
                <?= Html::img($v['url'], ['class' => 'gallery-image']) ?>
            </div>
        
    <?php endforeach; ?>
             </div>
   
    

    
   
</div>
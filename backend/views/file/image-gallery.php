<?php

use yii\helpers\Html;
use yii\web\UrlManager;
use dosamigos\gallery\Gallery;
/* @var $this yii\web\View */
/* @var $model common\models\SitePage */

$this->title = 'Gallerie d\'images';
$this->params['breadcrumbs'][] = ['label' => 'Galerie - Accueil', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Galerie d'images</h1>

<hr>

<div class="container-fluid">
    
<?= $this->render('_formFile', [
    'upload' => $upload,
    'file' => $image
])
?>
    
<hr>    

<?= Gallery::widget(['items' => $items, 'options' => ['id' => 'gallery']]) ?>

</div>
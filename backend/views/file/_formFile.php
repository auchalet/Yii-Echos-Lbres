<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['id' => 'page-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>   

<div class="row">
    <div class="col-lg-4 col-md-">
        <?= $form->field($upload, 'imageFile')->fileInput()->label('Ajouter un fichier') ?>        
    </div>

    <div class="col-lg-4">
        <?= $form->field($file, 'name')->textInput(['maxlength' => true])->label('Nom') ?>
        
    </div>
    <div class="col-lg-4">
       <?php if(Yii::$app->user->can('seo')): ?>

        <?= $form->field($file, 'alt')->textInput(['maxlength' => true])->label('Balise alt') ?>

        <?php endif; ?>
    </div>




    <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
</div>


<?php ActiveForm::end(); ?>




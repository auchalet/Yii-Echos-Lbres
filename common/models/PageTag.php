<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_tag".
 *
 * @property integer $tag_id
 * @property integer $page_id
 *
 * @property Tag $tag
 * @property SitePage $page
 */
class PageTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'page_id'], 'required'],
            [['tag_id', 'page_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => 'Tag ID',
            'page_id' => 'Page ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(SitePage::className(), ['id' => 'page_id']);
    }
}

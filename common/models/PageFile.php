<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_file".
 *
 * @property integer $id
 * @property integer $id_upload_file
 *
 * @property UploadFile $idUploadFile
 * @property Page $id0
 */
class PageFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'file_id'], 'required'],
            [['page_id', 'file_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'ID Page',
            'file_id' => 'Id Upload File',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(UploadFile::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}

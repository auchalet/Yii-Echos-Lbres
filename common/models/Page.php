<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $allow_comment
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $content_id
 * @property integer $category_id
 * @property integer $user_id
 *
 * @property ModerationPage[] $moderationPages
 * @property User $user
 * @property PageCategory $category
 * @property PageContent $content
 * @property PageComment[] $pageComments
 * @property PageContent[] $pageContents
 * @property PageContentCorrection[] $pageContentCorrections
 * @property PageFile[] $pageFiles
 * @property UploadFile[] $idUploadFiles
 * @property PageSource[] $pageSources
 * @property Sources[] $ids
 * @property PageTag[] $pageTags
 * @property Tag[] $tags
 * @property PageVersion[] $pageVersions
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'meta_title', 'allow_comment', 'status', 'category_id', 'user_id'], 'required'],
            [['meta_description', 'meta_keywords'], 'string'],
            [['allow_comment', 'status', 'category_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 120],
            [['slug'], 'string', 'max' => 100],
            [['meta_title'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'allow_comment' => 'Autoriser les commentaires sur cette page',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
        ];
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModerationPages()
    {
        return $this->hasMany(ModerationPage::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PageCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(PageContent::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageComments()
    {
        return $this->hasMany(PageComment::className(), ['page_id' => 'id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContentCorrections()
    {
        return $this->hasMany(PageContentCorrection::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageFiles()
    {
        return $this->hasMany(PageFile::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUploadFiles()
    {
        return $this->hasMany(UploadFile::className(), ['id' => 'id_upload_file'])->viaTable('page_file', ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageSources()
    {
        return $this->hasMany(PageSource::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIds()
    {
        return $this->hasMany(Sources::className(), ['id' => 'id'])->viaTable('page_source', ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageTags()
    {
        return $this->hasMany(PageTag::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('page_tag', ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageVersions()
    {
        return $this->hasMany(PageVersion::className(), ['page_id' => 'id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_content".
 *
 * @property integer $id
 * @property string $titre_principal
 * @property string $sous_titre
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 * @property integer $page_id
 *
 * @property Page[] $pages
 * @property Page $page
 */
class PageContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body', 'page_id'], 'required'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['page_id'], 'integer'],
            [['titre_principal'], 'string', 'max' => 150],
            [['sous_titre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titre_principal' => 'Titre Principal',
            'sous_titre' => 'Sous Titre',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'page_id' => 'Page ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "upload_file".
 *
 * @property integer $id
 * @property string $name
 * @property string $filename
 * @property string $extension
 * @property string $alt
 * @property integer $size
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ArticleFile[] $articleFiles
 * @property Article[] $articles
 * @property PageFile[] $pageFiles
 * @property Page[] $ids
 */
class UploadFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upload_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['extension'], 'required'],
            [['alt'], 'string'],
            [['size'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['filename'], 'string', 'max' => 256],
            [['extension'], 'string', 'max' => 20],
            [['type'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'filename' => 'Filename',
            'extension' => 'Extension',
            'alt' => 'Alt',
            'size' => 'Size',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }    
    
    /**
     * @return Array Tableau des images
     */
    public static function findImages()
    {
        return static::find()->where(['LIKE', 'type', 'image'])->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleFiles()
    {
        return $this->hasMany(ArticleFile::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_file', ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageFiles()
    {
        return $this->hasMany(PageFile::className(), ['file_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['id' => 'id'])->viaTable('page_file', ['file_id' => 'id']);
    }
    
    
    /**
     * Ajoute un fichier
     * DEPLACER DANS CONTROLLER ?? Car c'est une fonction applicative
     * @param type $file
     * @param type $name
     * @param type $filename
     * @param type $alt
     * @return boolean
     */
   public function addFile($file, $name = null, $alt = null, $filename = null)
    {
        if($file != NULL) {
           //Upload File : id / name / filename / extension / alt / size / type / created_at / updated_at

            
            //Formattage du Nom du fichier (qui sera sauvegardé)
            if($filename!=NULL) {
                $this->filename = $filename;
            } else {
                $filenamelow = strtolower($file->name);

                $filename = preg_replace('/[^A-Za-z0-9\-.]/', '-', $filenamelow);

                $this->filename = $filename;            
            }            
            
            //Formattage du Nom (utilisé pour la recherche)
            if($name!=NULL) {
                $this->name = $name;
                $ext = explode('.', $file->name);
                $this->extension = $ext[1];
            } else {
                $name = explode('.', $file->name);                                
                $this->extension = $name[1];
                $this->name = str_replace('.'.$this->extension,'',$filename);                
            }
            
            

            
            
            //Formattage de la balise Alt
            if($alt!=NULL) {
                $this->alt = $alt;
            }
            
            
            //Ajout du type et de la taille
            $this->type = $file->type;
            $this->size = $file->size;

//            
//            echo '<br>UploadFile<br>';
//            var_dump($this);
//            var_dump($this->validate());die;
            if($this->save()) {                
                return $this->id;
            }
            else {
//                var_dump('hého!!');
                return false;
            }
            
            
        }
    }    
    
}

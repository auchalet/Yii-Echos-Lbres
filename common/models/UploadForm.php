<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * Faire un formulaire générique pour tous les fichiers
 * Adapter si uniquement Image (ou faire un autre Model)
 */


class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }
    
    public function upload($path, $name = null, $alt = null)
    {
        if ($this->validate()) {
            
            try {
                FileHelper::createDirectory ( $path, $mode = 509, $recursive = true );
                var_dump($this->imageFile->name);

                $filenamelow = strtolower($this->imageFile->name);

                
                var_dump($filenamelow);
                
                $filename = preg_replace('/[^A-Za-z0-9\-.]/', '-', $filenamelow);  
                $this->imageFile->name = $filename;

//                var_dump($this->imageFile);die;
                if(!file_exists($path . '/' . $this->imageFile->name)) {
                    
                    $this->imageFile->saveAs( $path . '/' . $this->imageFile->name);

                    //Ajouter nouvel UploadFile() dans la BD -- Manque plus qu'à save
                    $file = new UploadFile;

                    return $file->addFile($this->imageFile, $name, $alt);
                } else {
                    Yii::$app->getSession()->setFlash(
                        'error','Le fichier existe déjà'
                    );                      
                    
                    return false;
                }
                

                
                
                
            } catch (Exception $ex) {
                echo "Exception upload_file : ".$ex->getMessage();
            }

            
        } else {
            return false;
        }
    }
    
    public function uploadAvatar($path)
    {
        if ($this->validate()) {
            
            try {
                FileHelper::createDirectory ( $path, $mode = 509, $recursive = true );
                //var_dump(FileHelper::findFiles($path)); 
                $this->imageFile->saveAs( $path . '/avatar.' . $this->imageFile->extension);
                
                //Ajouter nouvel UploadFile() dans la BD -- Manque plus qu'à save
                $file = new UploadFile;
                
                $name = 'avatar';
                $filename = 'avatar.'.$this->imageFile->extension;
                return $file->addFile($this->imageFile, $name, $filename);
                
                
                
            } catch (Exception $ex) {
                echo "Exception upload_file : ".$ex->getMessage();
            }

            
        } else {
            return false;
        }
    }    
}
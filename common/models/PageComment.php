<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_comment".
 *
 * @property integer $id
 * @property string $message
 * @property integer $status
 * @property double $score
 * @property string $created_at
 * @property string $updated_at
 * @property integer $page_id
 * @property integer $user_id
 *
 * @property User $user
 * @property Page $page
 */
class PageComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'page_id', 'user_id'], 'integer'],
            [['score', 'page_id', 'user_id'], 'required'],
            [['score'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['message'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'status' => 'Status',
            'score' => 'Score',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'page_id' => 'Page ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}

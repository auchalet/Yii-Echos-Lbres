<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $title
 * @property string $created_at
 *
 * @property ArticleTag[] $articleTags
 * @property Article[] $articles
 * @property PageTag[] $pageTags
 * @property SitePage[] $pages
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
        ];
    }
    
    
    public static function getAll()
    {
        return static::find()->asArray()->all();
    }
    
    public static function getAllTitle()
    {
        return static::find()->select('title')->asArray()->all();
    }    
    
    public static function getByTitle($title) {
        
        if($tag = static::findOne(['title' => $title])) {
            return $tag;
        } else {
            return false;
        }
    }     
    
    public static function getById($title) {
        
        if($tag = static::findOne(['id' => $title])) {
            return $tag;
        } else {
            return false;
        }
    }      
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTags()
    {
        return $this->hasMany(ArticleTag::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_tag', ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageTags()
    {
        return $this->hasMany(PageTag::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(SitePage::className(), ['id' => 'page_id'])->viaTable('page_tag', ['tag_id' => 'id']);
    }
}

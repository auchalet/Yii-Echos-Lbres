<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\widgets;

use Yii;

use dosamigos\tinymce\TinyMce;
use dosamigos\tinymce\TinyMceAsset;
use dosamigos\tinymce\TinyMceLangAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;


class Editor extends TinyMce {
    
    protected function registerClientScript()
    {
        
        
        
        $this->language='fr_FR';
        $this->clientOptions=[
                'plugins' => [
                    "advlist autolink lists link charmap preview anchor image",
                    "searchreplace visualblocks code",
                    "media table contextmenu paste wordcount"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
                'images_upload_url' => Url::to(['/files/image-gallery']),
                'images_upload_base_path' => Yii::getAlias('@frontend').'/web/uploads/',
                'images_upload_credentials' => true,
                ];
        
        $this->clientOptions['file_browser_callback'] = "function(field_name, url, type, win) { win.document.getElementById(field_name).value = document.body;}";
        $this->clientOptions['visual'] = false;
   
        
        parent::registerClientScript();
       
    }
}
